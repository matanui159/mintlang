#ifndef MINT_MEM_H_
#define MINT_MEM_H_
#include "../config.h"

void* mint_mem_create(size_t size);
void mint_mem_destroy(void* mem);
void* mint_mem_resize(void* mem, size_t size);

#endif
