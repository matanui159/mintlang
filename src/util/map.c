#include "map.h"
#include <string.h>

#define FNV_OFFSET 0x811C9DC5
#define FNV_PRIME 0x01000193

typedef uint32_t map_hash_t;

typedef struct map_key_t {
   map_hash_t hash;
   mint_string_t key;
   void* value;
} map_key_t;

static void map_create(mint_map_t* map, size_t capacity, size_t log2) {
   map->log2 = log2;
   mint_array_create(&map->keys, sizeof(map_key_t));
   mint_array_add(&map->keys, capacity);
   mint_array_create(&map->values, map->values.value_size);
   mint_array_add(&map->values, capacity);
   map_key_t* keys = (void*)map->keys.data;
   for (size_t i = 0; i < map->keys.size; ++i) {
      keys[i].hash = 0;
      mint_string_create(&keys[i].key);
      keys[i].value = mint_array_get(&map->values, i);
   }
}

static map_hash_t map_hash(mint_string_t* key) {
   map_hash_t hash = FNV_OFFSET;
   for (size_t i = 0; i < key->size; ++i) {
      hash = (hash ^ (uint8_t)key->data[i]) * FNV_PRIME;
      if (hash == 0) {
         hash = 1;
      }
   }
   return hash;
}

static map_key_t* map_bucket(mint_map_t* map, map_hash_t hash) {
   return (map_key_t*)map->keys.data + hash % (map->keys.size - map->log2 + 1);
}

static map_key_t* map_find(mint_map_t* map, mint_string_t* key, map_hash_t hash) {
   map_key_t* entry = map_bucket(map, hash);
   map_key_t* end = entry + map->log2;
   do {
      MINT_EXPECT(entry->hash != hash);
      else {
         mint_string_t* ekey = &entry->key;
         MINT_EXPECT(ekey->size == key->size &&
                     memcmp(ekey->data, key->data, ekey->size) == 0) {
            return entry;
         }
      }
      ++entry;
   } while (entry != end);
   return NULL;
}

static void* map_add(mint_map_t* map, mint_string_t* key, map_hash_t hash) {
   map_key_t* entry = map_bucket(map, hash);
   map_key_t* end = entry + map->log2;
   for (; entry != end; ++entry) {
      if (entry->hash == 0) {
         ++map->size;
         entry->hash = hash;
         mint_string_add(&entry->key, key);
         return entry->value;
      }
   }
   return NULL;
}

void* mint_map_add(mint_map_t* map, mint_string_t* key) {
   map_hash_t hash = map_hash(key);
   map_key_t* entry = map_find(map, key, hash);
   if (entry != NULL) {
      return entry->value;
   }

   for (;;) {
      void* value = map_add(map, key, hash);
      if (value != NULL) {
         return value;
      }

      mint_map_t old = *map;
      map_create(map, old.keys.size * 2, old.log2 + 1);
      map_key_t* keys = (void*)map->keys.data;
      for (size_t i = 0; i < map->keys.size; ++i) {
         memcpy(map_add(map, &keys[i].key, keys[i].hash), keys[i].value,
                map->values.value_size);
      }
      mint_map_destroy(&old);
   }
}

void mint_map_remove(mint_map_t* map, mint_string_t* key) {
   map_key_t* entry = map_find(map, key, map_hash(key));
   if (entry == NULL) {
      return;
   }
   --map->size;
   entry->hash = 0;
   entry->key.size = 0;
}

void* mint_map_get(mint_map_t* map, mint_string_t* key) {
   map_key_t* entry = map_find(map, key, map_hash(key));
   if (entry == NULL) {
      return NULL;
   }
   return entry->value;
}

void mint_map_create(mint_map_t* map, size_t value_size) {
   map->size = 0;
   map->values.value_size = value_size;
   map_create(map, 16, 4);
}

void mint_map_destroy(mint_map_t* map) {
   map_key_t* keys = (void*)map->keys.data;
   for (size_t i = 0; i < map->keys.size; ++i) {
      mint_string_destroy(&keys[i].key);
   }
   mint_array_destroy(&map->values);
   mint_array_destroy(&map->keys);
}
