#include "string.h"
#include "mem.h"

void mint_string_create(mint_string_t* string) {
   mint_array_create(string, 1);
}

void mint_string_destroy(mint_string_t* string) {
   mint_array_destroy(string);
}

void mint_string_add(mint_string_t* string, mint_string_t* other) {
   char* value = mint_array_add(string, other->size);
   memcpy(value, other->data, other->size);
}

void mint_string_remove(mint_string_t* string, size_t index, size_t count) {
   mint_array_remove(string, index, count);
}
