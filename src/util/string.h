#ifndef MINT_STRING_H_
#define MINT_STRING_H_
#include "../config.h"
#include "array.h"
#include <string.h>

typedef mint_array_t mint_string_t;

void mint_string_create(mint_string_t* string);
void mint_string_destroy(mint_string_t* string);
void mint_string_add(mint_string_t* string, mint_string_t* other);
void mint_string_remove(mint_string_t* string, size_t index, size_t count);

#define MINT_STRING_RAW(_size, _data) (&(mint_string_t){.size = (_size), .data = (_data)})
#define MINT_STRING_CSTR(cstr) MINT_STRING_RAW(strlen(cstr), cstr)
#define M_SR MINT_STRING_RAW
#define M_SC MINT_STRING_CSTR

#endif
