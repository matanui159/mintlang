#ifndef MINT_ARRAY_H_
#define MINT_ARRAY_H_
#include "../config.h"

typedef struct mint_array_t {
   size_t value_size;
   size_t size;
   size_t capacity;
   char* data;
} mint_array_t;

void mint_array_create(mint_array_t* array, size_t value_size);
void mint_array_destroy(mint_array_t* array);
void* mint_array_add(mint_array_t* array, size_t count);
void mint_array_remove(mint_array_t* array, size_t index, size_t count);
void* mint_array_get(mint_array_t* array, size_t index);

#endif
