#include "array.h"
#include "mem.h"
#include <SDL2/SDL_bits.h>
#include <string.h>

void mint_array_create(mint_array_t* array, size_t value_size) {
   array->value_size = value_size;
   array->size = 0;
   array->capacity = 0;
   array->data = NULL;
}

void mint_array_destroy(mint_array_t* array) {
   mint_mem_destroy(array->data);
}

void* mint_array_add(mint_array_t* array, size_t count) {
   array->size += count;
   if (array->size > array->capacity) {
      array->capacity = (size_t)2 << SDL_MostSignificantBitIndex32((uint32_t)array->size);
      if (array->capacity < 8) {
         array->capacity = 8;
      }
      array->data = mint_mem_resize(array->data, array->capacity * array->value_size);
   }
   return mint_array_get(array, array->size - count);
}

void mint_array_remove(mint_array_t* array, size_t index, size_t count) {
   array->size -= count;
   char* dest = mint_array_get(array, index);
   memmove(dest, dest + count * array->value_size,
           (array->size - index) * array->value_size);
}

void* mint_array_get(mint_array_t* array, size_t index) {
   return array->data + index * array->value_size;
}
