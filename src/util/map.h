#ifndef MINT_MAP_H_
#define MINT_MAP_H_
#include "../config.h"
#include "array.h"
#include "string.h"

typedef struct mint_map_t {
   size_t size;
   size_t log2;
   mint_array_t keys;
   mint_array_t values;
} mint_map_t;

void mint_map_create(mint_map_t* map, size_t value_size);
void mint_map_destroy(mint_map_t* map);
void* mint_map_add(mint_map_t* map, mint_string_t* key);
void mint_map_remove(mint_map_t* map, mint_string_t* key);
void* mint_map_get(mint_map_t* map, mint_string_t* key);

#endif
