#include "mem.h"
#include <stdio.h>
#include <stdlib.h>

static void mem_check(void* mem) {
   if (mem == NULL) {
      fputs("Out of memory\n", stderr);
      abort();
   }
}

void* mint_mem_create(size_t size) {
   void* mem = malloc(size);
   mem_check(mem);
   return mem;
}

void mint_mem_destroy(void* mem) {
   free(mem);
}

void* mint_mem_resize(void* mem, size_t size) {
   mem = realloc(mem, size);
   mem_check(mem);
   return mem;
}
