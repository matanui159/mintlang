#include "build.h"
#include "location.h"

#define BUILD_TRY(cond) \
   if (!cond) return false

typedef enum build_value_type_t {
   BUILD_VALUE_REG,
   BUILD_VALUE_NUMBER,
   BUILD_VALUE_STRING
} build_value_type_t;

typedef struct build_value_t {
   build_value_type_t type;
   int16_t reg;
   union {
      double number;
      mint_string_t string;
   } u;
} build_value_t;

// static void value_destroy(build_value_t* value) {
//    if (value->type == BUILD_VALUE_STRING) {
//       mint_string_destroy(&value->u.string);
//    }
// }

static void build_instr2(mint_builder_t* builder, mint_opcode_t opcode, uint8_t arg0,
                         uint16_t arg1) {
   mint_instr_t* instr = mint_array_add(&builder->instrs, 1);
   *instr = (uint32_t)((uint8_t)opcode | arg0 << 8 | arg1 << 16);
}

static void build_instr3(mint_builder_t* builder, mint_opcode_t opcode, uint8_t arg0,
                         uint8_t arg1, uint8_t arg2) {
   build_instr2(builder, opcode, arg0, (uint16_t)(arg1 | arg2 << 8));
}

// static mint_token_t* build_token(mint_builder_t* builder) {
//    mint_token_t* token = mint_lexer_next(builder);
//    uint16_t line = (uint16_t)token->loc.line;
//    if (line != builder->line) {
//       build_instr2(builder, MINT_OPCODE_LINE, 0, line);
//       builder->line = line;
//    }
//    return token;
// }

// static bool build_value(mint_builder_t* builder, build_value_t* value) {
//    mint_token_t* token = build_token(builder);
//    switch (token->type) {
//       case MINT_TOKEN_NUMBER:
//          value->type =
//    }
// }

void mint_builder_create(mint_builder_t* builder) {
   mint_lexer_create(&builder->lexer);
   mint_array_create(&builder->instrs, sizeof(mint_instr_t));
}

void mint_builder_destroy(mint_builder_t* builder) {
   mint_array_destroy(&builder->instrs);
   mint_lexer_destroy(&builder->lexer);
}

mint_instr_t* mint_build(mint_builder_t* builder, mint_string_t* file,
                         mint_string_t* input, size_t* size) {
   builder->instrs.size = 0;
   builder->line = 1;
   mint_lexer_start(&builder->lexer, file, input);
   build_instr3(builder, MINT_OPCODE_MOV, 0x11, 0x22, 0x33);
   build_instr2(builder, MINT_OPCODE_MOV, 0x11, 0x2233);
   *size = builder->instrs.size;
   return (void*)builder->instrs.data;
}
