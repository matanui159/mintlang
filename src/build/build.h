#ifndef MINT_BUILD_H_
#define MINT_BUILD_H_
#include "../config.h"
#include "lexer.h"
#include "../util/array.h"
#include "../util/string.h"

typedef enum mint_opcode_t {
   MINT_OPCODE_MOV,
   MINT_OPCODE_MOVI,
   MINT_OPCODE_MOVF,
   MINT_OPCODE_MOVS,

   MINT_OPCODE_LINE
} mint_opcode_t;

typedef uint32_t mint_instr_t;

typedef struct mint_builder_t {
   mint_lexer_t lexer;
   mint_array_t instrs;
   uint16_t line;
   int16_t stack;
} mint_builder_t;

void mint_builder_create(mint_builder_t* builder);
void mint_builder_destroy(mint_builder_t* builder);
mint_instr_t* mint_build(mint_builder_t* builder, mint_string_t* file,
                         mint_string_t* input, size_t* size);

#endif
