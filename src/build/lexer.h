#ifndef MINT_LEXER_H_
#define MINT_LEXER_H_
#include "../config.h"
#include "keywords.h"
#include "location.h"
#include "../util/string.h"

typedef uint16_t mint_token_tag_t;
#define MINT_TOKEN_TAG(a, b) ((mint_token_tag_t)((a) | (b) << 8))

#define KEYWORD_CALLBACK(kw) MINT_TOKEN_KW_##kw,
typedef enum mint_token_type_t {
   MINT_TOKEN_EOF,
   MINT_TOKEN_NAME,
   MINT_TOKEN_NUMBER,
   MINT_TOKEN_STRING,
   MINT_TOKEN_SYMBOL,
   MINT_KEYWORDS(KEYWORD_CALLBACK)
} mint_token_type_t;
#undef KEYWORD_CALLBACK

typedef struct mint_token_t {
   mint_location_t loc;
   mint_token_type_t type;
   union {
      double number;
      mint_string_t string;
      char symbol[3];
      mint_token_tag_t tag;
   } u;
} mint_token_t;

typedef struct mint_lexer_t {
   mint_string_t input;
   int ch;
   mint_token_t token;
   mint_string_t buffer;
   bool peeked;
} mint_lexer_t;

const char* mint_token_name(mint_token_t* token);
void mint_lexer_create(mint_lexer_t* lexer);
void mint_lexer_destroy(mint_lexer_t* lexer);
void mint_lexer_start(mint_lexer_t* lexer, mint_string_t* file, mint_string_t* input);
mint_token_t* mint_lexer_peek(mint_lexer_t* lexer);
mint_token_t* mint_lexer_next(mint_lexer_t* lexer);

#define M_TT MINT_TOKEN_TAG

#endif
