#include "location.h"
#include <SDL2/SDL_atomic.h>
#include <stdio.h>

static SDL_SpinLock g_errorlock = 0;

void mint_location_start(mint_location_t* loc, mint_string_t* file) {
   loc->file = *file;
   loc->line = 1;
}

void mint_location_error(mint_location_t* loc, const char* error, ...) {
   va_list args;
   va_start(args, error);
   SDL_AtomicLock(&g_errorlock);
   fwrite(loc->file.data, loc->file.size, 1, stderr);
   fprintf(stderr, ":%u: ", loc->line);
   vfprintf(stderr, error, args);
   fputc('\n', stderr);
   SDL_AtomicUnlock(&g_errorlock);
   va_end(args);
}
