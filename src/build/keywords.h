#ifndef MINT_KEYWORDS_H_
#define MINT_KEYWORDS_H_
#include "../config.h"

#define MINT_KEYWORDS(callback) callback(not) callback(or) callback(and)

#endif
