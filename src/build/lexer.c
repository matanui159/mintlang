#include "lexer.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define ERROR_SEOF "Unexpected end of file, expected closing quote"

static int lexer_peekchar(mint_lexer_t* lexer) {
   mint_string_t* input = &lexer->input;
   if (lexer->ch == EOF && input->size > 0) {
      lexer->ch = *input->data;
      ++input->data;
      --input->size;
   }
   return lexer->ch;
}

static int lexer_nextchar(mint_lexer_t* lexer) {
   int ch = lexer_peekchar(lexer);
   lexer->ch = EOF;
   if (ch == '\n') {
      ++lexer->token.loc.line;
   }
   return ch;
}

static bool lexer_isname(int ch) {
   return ch > 0x7F || isalpha(ch) || ch == '_' || ch == '$';
}

const char* mint_token_name(mint_token_t* token) {
#define KEYWORD_CALLBACK(kw) \
   case MINT_TOKEN_KW_##kw:  \
      return #kw;
   switch (token->type) {
      case MINT_TOKEN_EOF:
         return "end of file";
      case MINT_TOKEN_NAME:
         return "name";
      case MINT_TOKEN_NUMBER:
         return "number";
      case MINT_TOKEN_STRING:
         return "string";
      case MINT_TOKEN_SYMBOL:
         return token->u.symbol;
         MINT_KEYWORDS(KEYWORD_CALLBACK)
      default:
         return NULL;
   }
#undef KEYWORD_CALLBACK
}

void mint_lexer_create(mint_lexer_t* lexer) {
   mint_string_create(&lexer->buffer);
}

void mint_lexer_destroy(mint_lexer_t* lexer) {
   mint_string_destroy(&lexer->buffer);
}

void mint_lexer_start(mint_lexer_t* lexer, mint_string_t* file, mint_string_t* input) {
   lexer->input = *input;
   lexer->ch = EOF;
   lexer->peeked = false;
   mint_location_start(&lexer->token.loc, file);
}

mint_token_t* mint_lexer_peek(mint_lexer_t* lexer) {
   mint_token_t* token = &lexer->token;
   if (lexer->peeked) {
      return token;
   }

   int ch;
   for (;;) {
      while (isspace(ch = lexer_peekchar(lexer))) {
         lexer_nextchar(lexer);
      }
      if (ch != '#') {
         break;
      }
      while ((ch = lexer_nextchar(lexer)) != '\n') continue;
   }

   if (ch == EOF) {
      token->type = MINT_TOKEN_EOF;

   } else if (lexer_isname(ch)) {
      mint_string_t* buffer = &lexer->buffer;
      buffer->size = 0;
      do {
         char c = (char)lexer_nextchar(lexer);
         mint_string_add(buffer, M_SR(1, &c));
         ch = lexer_peekchar(lexer);
      } while (lexer_isname(ch) || isdigit(ch));
      token->type = MINT_TOKEN_NAME;
      token->u.string = *buffer;

   } else if (isdigit(ch)) {
      double num = 0;
      double den = 0;
      do {
         lexer_nextchar(lexer);
         if (ch == '.') {
            if (den != 0) {
               mint_location_error(&token->loc,
                                   "Unexpected decimal point, one already provided");
               return NULL;
            }
            den = 1;
         } else {
            den *= 10;
            num = num * 10 + (ch - '0');
         }
         ch = lexer_peekchar(lexer);
      } while (isdigit(ch) || ch == '.');
      if (den != 0) {
         num /= den;
      }
      token->type = MINT_TOKEN_NUMBER;
      token->u.number = num;

   } else if (ch == '"' || ch == '\'') {
      int quote = lexer_nextchar(lexer);
      mint_string_t* buffer = &lexer->buffer;
      buffer->size = 0;
      while ((ch = lexer_nextchar(lexer)) != quote) {
         if (ch == EOF) {
            mint_location_error(&token->loc, ERROR_SEOF);
            return NULL;
         }

         char c;
         if (ch == '\\') {
            ch = lexer_nextchar(lexer);
            switch (ch) {
               case EOF:
                  mint_location_error(&token->loc, ERROR_SEOF);
                  return NULL;
               case 'b':
                  c = '\b';
                  break;
               case 't':
                  c = '\t';
                  break;
               case 'n':
                  c = '\n';
                  break;
               case 'r':
                  c = '\r';
                  break;
               default:
                  c = (char)ch;
                  break;
            }
         } else {
            c = (char)ch;
         }
         mint_string_add(buffer, M_SR(1, &c));
      }
      token->type = MINT_TOKEN_STRING;
      token->u.string = *buffer;

   } else if (strchr("=+-*/%!?.:,<>()[]{}", ch) != NULL) {
      char* symbol = token->u.symbol;
      symbol[0] = (char)lexer_nextchar(lexer);
      if (strchr("=+-*/%!<>", ch) != NULL && lexer_peekchar(lexer) == '=') {
         symbol[1] = (char)lexer_nextchar(lexer);
         symbol[2] = '\0';
      } else {
         symbol[1] = '\0';
      }
      token->type = MINT_TOKEN_SYMBOL;

   } else {
      mint_location_error(&token->loc, "Unknown character '%c'", ch);
      return NULL;
   }

   lexer->peeked = true;
   return token;
}

mint_token_t* mint_lexer_next(mint_lexer_t* lexer) {
   mint_token_t* token = mint_lexer_peek(lexer);
   lexer->peeked = false;
   return token;
}
