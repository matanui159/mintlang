#ifndef MINT_LINE_H_
#define MINT_LINE_H_
#include "../config.h"
#include "../util/string.h"

typedef struct mint_location_t {
   mint_string_t file;
   unsigned line;
} mint_location_t;

void mint_location_start(mint_location_t* loc, mint_string_t* file);
void mint_location_error(mint_location_t* loc, const char* error, ...);

#endif
