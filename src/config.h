#ifndef MINT_CONFIG_H_
#define MINT_CONFIG_H_
#include <SDL2/SDL_log.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define MINT_LOG SDL_LOG_CATEGORY_APPLICATION

#ifdef __GNUC__
#define MINT_EXPECT(cond) if (__builtin_expect((cond), true))
#else
#define MINT_EXPECT(cond) if (cond)
#endif

#endif
