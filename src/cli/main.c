#include "../build/build.h"
#include <SDL2/SDL_log.h>
#include <SDL2/SDL_main.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);
   if (argc != 2) {
      fprintf(stderr, "Usage: %s FILE\n", argv[0]);
      return EXIT_FAILURE;
   }

   FILE* file = fopen(argv[0], "rb");
   if (file == NULL) {
      perror("Failed to open file");
      return EXIT_FAILURE;
   }

   mint_string_t input;
   mint_string_create(&input);
   fseek(file, 0, SEEK_END);
   mint_array_add(&input, (size_t)ftell(file));
   fseek(file, 0, SEEK_SET);
   size_t read = fread(input.data, 1, input.size, file);
   fclose(file);
   if (read < input.size) {
      mint_string_destroy(&input);
      fputs("Failed to read from file", stderr);
      return EXIT_FAILURE;
   }

   mint_builder_t builder;
   mint_builder_create(&builder);
   size_t size;
   mint_instr_t* instrs = mint_build(&builder, M_SC(argv[0]), &input, &size);
   mint_string_destroy(&input);
   if (instrs == NULL) {
      mint_builder_destroy(&builder);
      return EXIT_FAILURE;
   }

   for (size_t i = 0; i < size; ++i) {
      printf("%04X: %08X\n", (uint16_t)i, instrs[i]);
   }
   mint_builder_destroy(&builder);
   return EXIT_SUCCESS;
}
