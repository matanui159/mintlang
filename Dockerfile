FROM ubuntu

RUN apt-get update && apt-get install -y \
   python3-pip ninja-build \
   gcc libsdl2-dev curl \
   gcc-mingw-w64 mingw-w64-tools make \
   clang-format git
RUN pip3 install meson gcovr

ENV SDL_VERSION=2.0.10
RUN curl -sL https://libsdl.org/release/SDL2-devel-$SDL_VERSION-mingw.tar.gz | \
   tar xz && \
   make -C SDL2-$SDL_VERSION cross CROSS_PATH=/usr && \
   rm -r SDL2-$SDL_VERSION
